package com.mycila.event.internal;

import org.junit.Assert;
import org.junit.Test;

import com.mycila.event.Event;
import com.mycila.event.Reachability;
import com.mycila.event.Subscriber;
import com.mycila.event.Subscription;
import com.mycila.event.Topics;
import com.mycila.event.annotation.Reference;

public class SubscriptionListTest {

    @Test
    public void addWeakReference() throws Exception {
        SubscriptionList list = new SubscriptionList();

        Subscription<Object> subscription = Subscription.create(Topics.any(), Object.class,
                new WeakSubscriber<Object>());

        list.add(subscription);

        Assert.assertFalse(list.isEmpty());
        Assert.assertTrue(list.iterator().hasNext());
        Assert.assertNotNull(list.iterator().next());

        System.gc();
        System.gc();
        System.gc();

        Assert.assertFalse(list.iterator().hasNext());
        Assert.assertTrue(list.isEmpty());
    }

    @Test
    public void addHardReference() throws Exception {
        SubscriptionList list = new SubscriptionList();

        Subscription<Object> subscription = Subscription.create(Topics.any(), Object.class,
                new HardSubscriber<Object>());

        list.add(subscription);

        Assert.assertFalse(list.isEmpty());
        Assert.assertTrue(list.iterator().hasNext());
        Assert.assertNotNull(list.iterator().next());

        System.gc();
        System.gc();
        System.gc();

        Assert.assertTrue(list.iterator().hasNext());
        Assert.assertNotNull(list.iterator().next());
    }

    @Reference(Reachability.WEAK)
    private static class WeakSubscriber<T> implements Subscriber<T> {
        @Override
        public void onEvent(Event<T> event) throws Exception {
        }
    }

    @Reference(Reachability.HARD)
    private static class HardSubscriber<T> implements Subscriber<T> {
        @Override
        public void onEvent(Event<T> event) throws Exception {
        }
    }

}
