package com.mycila.event;

import org.junit.Assert;
import org.junit.Test;

import com.mycila.event.Event;
import com.mycila.event.Reachability;
import com.mycila.event.Subscriber;
import com.mycila.event.Subscription;
import com.mycila.event.Topics;
import com.mycila.event.annotation.Reference;

public class SubscriptionTest {

    @Test
    public void hardlyReferenced() {
        HardSubscriber hard = new HardSubscriber();

        Subscription<Object> subscription = Subscription.create(Topics.any(), Object.class, hard);

        hard = null;

        System.gc();

        Assert.assertNotNull(subscription.getSubscriber());
    }

    @Test
    public void weaklyReferenced() {
        WeakSubscriber weak = new WeakSubscriber();

        Subscription<Object> subscription = Subscription.create(Topics.any(), Object.class, weak);

        weak = null;

        System.gc();

        Assert.assertNull(subscription.getSubscriber());

    }

    @Reference(Reachability.HARD)
    private static class HardSubscriber implements Subscriber<Object> {
        @Override
        public void onEvent(Event<Object> event) throws Exception {
        }
    }

    @Reference(Reachability.WEAK)
    private static class WeakSubscriber implements Subscriber<Object> {
        @Override
        public void onEvent(Event<Object> event) throws Exception {
        }
    }

}
