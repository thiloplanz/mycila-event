/**
 * Copyright (C) 2010 Mycila <mathieu.carbou@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mycila.event.internal;

import static com.google.common.collect.Iterators.filter;
import static com.google.common.collect.Iterators.transform;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.mycila.event.Subscriber;
import com.mycila.event.Subscription;

/**
 * @author Mathieu Carbou (mathieu.carbou@gmail.com)
 */
final class SubscriptionList implements Iterable<Subscription<?>> {

    private final CopyOnWriteArrayList<Subscription<?>> subscriptions = new CopyOnWriteArrayList<Subscription<?>>();

    public boolean add(Subscription<?> subscription) {
        return subscriptions.add(subscription);
    }

    public boolean isEmpty() {
        return subscriptions.isEmpty();
    }

    public int size() {
        return subscriptions.size();
    }

    @Override
    public Iterator<Subscription<?>> iterator() {
        return filter(transform(subscriptions.iterator(), TRANSFORMER), FILTER);
    }

    public void remove(Subscription<?> subscription) {
        for (Subscription<?> actual : subscriptions) {
            if (actual.equals(subscription)) {
                subscriptions.remove(actual);
            }
        }
    }

    private static final Predicate<Subscription<?>> FILTER = new Predicate<Subscription<?>>() {
        @Override
        public boolean apply(Subscription<?> input) {
            return input != null;
        }
    };

    private final Function<Subscription<?>, Subscription<?>> TRANSFORMER = new Function<Subscription<?>, Subscription<?>>() {
        @Override
        public Subscription<?> apply(Subscription<?> subscription) {
            Subscriber<?> subscriber = subscription.getSubscriber();
            if (subscriber == null) {
                subscriptions.remove(subscription);
                return null;
            }

            return subscription;
        }

    };
}
